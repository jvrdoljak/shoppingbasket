# Shopping basket
## Overview
Here is shopping basket component that allows customers to add products and get total value of basket with all products and all applicable discounts.
Component can be easily integrated with typical web shop.

## Shopping basket scenario
Available products:
1. Butter, 0.80$
2. Milk, 1.15$
3. Bread, 1.00$
4. ...

Available discounts:
1. if you have 2 butters, you will get 50% off on one bread
2. if you have 3 milks, you will get 4th milk free
3. ...

Results:
1. you have one butter, one milk and one bread, then total value will be 2.95$
2. you have two butters and two bread, total value will be 3.10$
3. you have four milks, total value will be 3.45$
4. you have two butters, one bread and 8 milks, total value is 9.00$

## Solution explanation
Given solution has products, discounts, basket and fake user. 
User could have only one active basket.
There are two types of discount: 
- discount
- gift

### Add new product to basket
On each new product in basket, program calculates total value and check for discounts which this product can trigger and discounts which can be applied on this product. 
When products trigger discounts, it will be applied in applicable discounts, for that basket.

### Remove product from basket
On remove, program again calculates total value, but this time the calculation goes backwards.

## Installation
1. Clone project 
2. Create database and paste connection string to *Web.ShoppingBasket > appsettings.json > DatabaseConnection*
3. Using Package Manager Console call **Update-Database** *(Default project: App.ShoppingBasket)*
4. Start iis express from web shopping basket.

## Technologies 
- ASP.NET Core 3.1
- Entity Framework Core 3.1
- Swagger

## Prerequisites
- SQL Server
- Visual Studio 2019





