using App.ShoppingBasket.Enum;
using App.ShoppingBasket.Models;
using App.ShoppingBasket.Services;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Permissions;
using Xunit;

namespace Test.ShoppingBasket
{
    public class BasketServiceTest
    {
        [Fact]
        public void BasketService_Create_ShouldReturn_OpenedBasket()
        {
            var basketService = new BasketService(new DiscountService());

            var b = basketService.Create(1);

            Assert.False(b.IsClosed);
        }

        [Fact]
        public void BasketService_Create_ShouldReturn_ZeroTotalValue()
        {
            var basketService = new BasketService(new DiscountService());

            var b = basketService.Create(1);

            Assert.Equal(0, b.TotalValue);
        }

        [Fact]
        public void BasketService_CreateBasketProduct_ShouldReturn_ProductCountOne()
        {
            var basketService = new BasketService(new DiscountService());

            var b = basketService.CreateBasketProduct(1, 1, true);

            Assert.Equal(1, b.ProductCount);
        }

        /// <summary>
        /// When there is no one applicable discount and basket is empty.
        /// Then basket should return total value exact as product price.
        /// </summary>
        [Fact]
        public void BasketService_AddProductToBasket_ShouldReturn_TotalValue()
        {
            var basketService = new BasketService(new DiscountService());

            Basket basket = new Basket
            {
                Id = 1,
                TotalValue = 0,
                IsClosed = false
            };

            Product product = new Product
            {
                Id = 1,
                Name = "Milk",
                Price = 1.15
            };

            var b = basketService.AddProductToBasket(product, basket, false);

            Assert.Equal(product.Price, basket.TotalValue);
        }

        [Fact]
        public void BasketService_AddProductToBasket_ShouldAddUsedDiscount()
        {
            var basketService = new BasketService(new DiscountService());

            Basket basket = new Basket
            {
                Id = 1,
                TotalValue = 0,
                IsClosed = false
            };

            Product product = new Product
            {
                Id = 1,
                Name = "Milk",
                Price = 1.15
            };

            Discount discount = new Discount
            {
                Id = 1,
                Rate = 0.4,
                DiscountProductId = 1,
                DiscountProduct = product,
                IsActive = true,
                TriggerProductId = 1,
                TriggerProduct = product,
                TriggerProductCount = 1,
                Type = DiscountTypeEnum.Discount
            };

            product.ApplicableDiscounts.Add(discount);
            product.DiscountsForTriggering.Add(discount);


            var b = basketService.AddProductToBasket(product, basket, false);

            Assert.True(basket.ApplicableDiscounts.FirstOrDefault().IsUsed);
        }

        [Fact]
        public void BasketService_AddProductToBasket_ShouldAddUnusedDiscount()
        {
            var basketService = new BasketService(new DiscountService());

            Basket basket = new Basket
            {
                Id = 1,
                TotalValue = 0,
                IsClosed = false
            };

            Product product = new Product
            {
                Id = 1,
                Name = "Milk",
                Price = 1.15
            };

            Product product2 = new Product
            {
                Id = 2,
                Name = "Milk",
                Price = 1.15
            };

            Discount discount = new Discount
            {
                Id = 1,
                Rate = 0.4,
                DiscountProductId = 2,
                DiscountProduct = product2,
                IsActive = true,
                TriggerProductId = 1,
                TriggerProduct = product,
                TriggerProductCount = 1,
                Type = DiscountTypeEnum.Discount
            };

            product2.ApplicableDiscounts.Add(discount);
            product.DiscountsForTriggering.Add(discount);


            var b = basketService.AddProductToBasket(product, basket, false);

            Assert.False(basket.ApplicableDiscounts.FirstOrDefault().IsUsed);
        }

        [Fact]
        public void BasketService_AddProductToBasket_ShouldApplyDiscount()
        {
            var basketService = new BasketService(new DiscountService());

            Basket basket = new Basket
            {
                Id = 1,
                TotalValue = 0,
                IsClosed = false,
                UserId = 1,
                ApplicableDiscounts = new List<ApplicableDiscount>(),
                BasketProducts = new List<BasketProduct>()
            };

            Product product = new Product
            {
                Id = 1,
                Name = "Milk",
                Price = 1,
                ApplicableDiscounts = new List<Discount>(),
                BasketProducts = new List<BasketProduct>(),
                DiscountsForTriggering = new List<Discount>()
            };

            Product product2 = new Product
            {
                Id = 2,
                Name = "Milk",
                Price = 1,
                ApplicableDiscounts = new List<Discount>(),
                BasketProducts = new List<BasketProduct>(),
                DiscountsForTriggering = new List<Discount>()
            };

            Discount discount = new Discount
            {
                Id = 1,
                Rate = 0.4,
                DiscountProductId = 2,
                DiscountProduct = product2,
                IsActive = true,
                TriggerProductId = 1,
                TriggerProduct = product,
                TriggerProductCount = 1,
                Type = DiscountTypeEnum.Discount
            };

            product2.ApplicableDiscounts.Add(discount);
            product.DiscountsForTriggering.Add(discount);

            basket = basketService.AddProductToBasket(product2, basket, false);
            basket = basketService.AddProductToBasket(product, basket, false);

            Assert.Equal(1.6, basket.TotalValue);
        }

        [Fact]
        public void BasketService_AddProductToBasket_ShouldApplyUnusedDiscount()
        {
            var basketService = new BasketService(new DiscountService());

            Basket basket = new Basket
            {
                Id = 1,
                TotalValue = 0,
                IsClosed = false,
                UserId = 1,
                ApplicableDiscounts = new List<ApplicableDiscount>(),
                BasketProducts = new List<BasketProduct>()
            };

            Product product = new Product
            {
                Id = 1,
                Name = "Milk",
                Price = 1,
                ApplicableDiscounts = new List<Discount>(),
                BasketProducts = new List<BasketProduct>(),
                DiscountsForTriggering = new List<Discount>()
            };

            Product product2 = new Product
            {
                Id = 2,
                Name = "Milk",
                Price = 1,
                ApplicableDiscounts = new List<Discount>(),
                BasketProducts = new List<BasketProduct>(),
                DiscountsForTriggering = new List<Discount>()
            };

            Discount discount = new Discount
            {
                Id = 1,
                Rate = 0.4,
                DiscountProductId = 2,
                DiscountProduct = product2,
                IsActive = true,
                TriggerProductId = 1,
                TriggerProduct = product,
                TriggerProductCount = 1,
                Type = DiscountTypeEnum.Discount
            };

            product2.ApplicableDiscounts.Add(discount);
            product.DiscountsForTriggering.Add(discount);

            basket = basketService.AddProductToBasket(product, basket, false);
            basket = basketService.AddProductToBasket(product2, basket, false);

            Assert.Equal(1.6, basket.TotalValue);
        }

        [Fact]
        public void BasketService_RemoveProductFromBasket_TotalValue()
        {
            var basketService = new BasketService(new DiscountService());

            Product product = new Product
            {
                Id = 1,
                Name = "Milk",
                Price = 1,
                ApplicableDiscounts = new List<Discount>(),
                BasketProducts = new List<BasketProduct>(),
                DiscountsForTriggering = new List<Discount>()
            };

            BasketProduct basketProduct = new BasketProduct
            {
                Id = 1,
                IsGift = false,
                BasketId = 1,
                ProductId = 1,
                Product = product,
                ProductCount = 1
            };

            Basket basket = new Basket
            {
                Id = 1,
                TotalValue = 10,
                IsClosed = false,
                UserId = 1,
                ApplicableDiscounts = new List<ApplicableDiscount>(),
                BasketProducts = new List<BasketProduct>()
            };

            basket.BasketProducts.Add(basketProduct);

            basket = basketService.RemoveProductFromBasket(product, basket, false);

            Assert.Equal(9, basket.TotalValue);
        }

        [Fact]
        public void BasketService_RemoveProductFromBasket_RemoveUnusedDiscount()
        {
            var basketService = new BasketService(new DiscountService());

            Product product = new Product
            {
                Id = 1,
                Name = "Milk",
                Price = 1,
                ApplicableDiscounts = new List<Discount>(),
                BasketProducts = new List<BasketProduct>(),
                DiscountsForTriggering = new List<Discount>()
            };

            Product product2 = new Product
            {
                Id = 2,
                Name = "Milk",
                Price = 1,
                ApplicableDiscounts = new List<Discount>(),
                BasketProducts = new List<BasketProduct>(),
                DiscountsForTriggering = new List<Discount>()
            };

            Discount discount = new Discount
            {
                Id = 1,
                Rate = 0.4,
                DiscountProductId = 2,
                DiscountProduct = product2,
                IsActive = true,
                TriggerProductId = 1,
                TriggerProduct = product,
                TriggerProductCount = 1,
                Type = DiscountTypeEnum.Discount
            };

            product.DiscountsForTriggering.Add(discount);
            product2.ApplicableDiscounts.Add(discount);

            BasketProduct basketProduct = new BasketProduct
            {
                Id = 1,
                IsGift = false,
                BasketId = 1,
                ProductId = 1,
                Product = product,
                ProductCount = 1
            };

            Basket basket = new Basket
            {
                Id = 1,
                TotalValue = 10,
                IsClosed = false,
                UserId = 1,
                ApplicableDiscounts = new List<ApplicableDiscount>(),
                BasketProducts = new List<BasketProduct>()
            };

            basket.BasketProducts.Add(basketProduct);
            basket.ApplicableDiscounts.Add(new ApplicableDiscount
            {
                Id = 1,
                Basket = basket,
                BasketId = 1,
                Discount = discount,
                DiscountId = 1,
                IsUsed = false
            });

            basket = basketService.RemoveProductFromBasket(product, basket, false);

            Assert.False(basket.ApplicableDiscounts.Any());
        }

        [Fact]
        public void BasketService_RemoveProductFromBasket_RemoveUsedDiscount()
        {
            var basketService = new BasketService(new DiscountService());

            Product product = new Product
            {
                Id = 1,
                Name = "Milk",
                Price = 1,
                ApplicableDiscounts = new List<Discount>(),
                BasketProducts = new List<BasketProduct>(),
                DiscountsForTriggering = new List<Discount>()
            };

            Product product2 = new Product
            {
                Id = 2,
                Name = "Milk",
                Price = 1,
                ApplicableDiscounts = new List<Discount>(),
                BasketProducts = new List<BasketProduct>(),
                DiscountsForTriggering = new List<Discount>()
            };

            Discount discount = new Discount
            {
                Id = 1,
                Rate = 0.4,
                DiscountProductId = 2,
                DiscountProduct = product2,
                IsActive = true,
                TriggerProductId = 1,
                TriggerProduct = product,
                TriggerProductCount = 1,
                Type = DiscountTypeEnum.Discount
            };

            product.DiscountsForTriggering.Add(discount);
            product2.ApplicableDiscounts.Add(discount);

            Basket basket = new Basket
            {
                Id = 1,
                TotalValue = 10,
                IsClosed = false,
                UserId = 1,
                ApplicableDiscounts = new List<ApplicableDiscount>(),
                BasketProducts = new List<BasketProduct>()
            };

            var basketProduct = new BasketProduct
            {
                Id = 1,
                IsGift = false,
                BasketId = 1,
                ProductId = 1,
                Product = product,
                ProductCount = 1
            };

            product.BasketProducts.Add(basketProduct);
            basket.BasketProducts.Add(basketProduct);

            var basketProduct2 = new BasketProduct
            {
                Id = 2,
                IsGift = false,
                BasketId = 1,
                ProductId = 2,
                Product = product2,
                ProductCount = 1
            };

            product2.BasketProducts.Add(basketProduct2);
            basket.BasketProducts.Add(basketProduct2);

            basket.ApplicableDiscounts.Add(new ApplicableDiscount
            {
                Id = 1,
                Basket = basket,
                BasketId = 1,
                Discount = discount,
                DiscountId = 1,
                IsUsed = true
            });

            basket = basketService.RemoveProductFromBasket(product, basket, false);

            Assert.False(basket.ApplicableDiscounts.Any());
        }

        [Fact]
        public void BasketService_RemoveProductFromBasket_ChangeToUnusedDiscount()
        {
            var basketService = new BasketService(new DiscountService());

            Product product = new Product
            {
                Id = 1,
                Name = "Milk",
                Price = 1,
                ApplicableDiscounts = new List<Discount>(),
                BasketProducts = new List<BasketProduct>(),
                DiscountsForTriggering = new List<Discount>()
            };

            Product product2 = new Product
            {
                Id = 2,
                Name = "Milk",
                Price = 1,
                ApplicableDiscounts = new List<Discount>(),
                BasketProducts = new List<BasketProduct>(),
                DiscountsForTriggering = new List<Discount>()
            };

            Discount discount = new Discount
            {
                Id = 1,
                Rate = 0.4,
                DiscountProductId = 2,
                DiscountProduct = product2,
                IsActive = true,
                TriggerProductId = 1,
                TriggerProduct = product,
                TriggerProductCount = 1,
                Type = DiscountTypeEnum.Discount
            };

            product.DiscountsForTriggering.Add(discount);
            product2.ApplicableDiscounts.Add(discount);

            Basket basket = new Basket
            {
                Id = 1,
                TotalValue = 10,
                IsClosed = false,
                UserId = 1,
                ApplicableDiscounts = new List<ApplicableDiscount>(),
                BasketProducts = new List<BasketProduct>()
            };

            var basketProduct = new BasketProduct
            {
                Id = 1,
                IsGift = false,
                BasketId = 1,
                ProductId = 1,
                Product = product,
                ProductCount = 1
            };

            product.BasketProducts.Add(basketProduct);
            basket.BasketProducts.Add(basketProduct);

            var basketProduct2 = new BasketProduct
            {
                Id = 2,
                IsGift = false,
                BasketId = 1,
                ProductId = 2,
                Product = product2,
                ProductCount = 1
            };

            product2.BasketProducts.Add(basketProduct2);
            basket.BasketProducts.Add(basketProduct2);

            basket.ApplicableDiscounts.Add(new ApplicableDiscount
            {
                Id = 1,
                Basket = basket,
                BasketId = 1,
                Discount = discount,
                DiscountId = 1,
                IsUsed = true
            });

            basket = basketService.RemoveProductFromBasket(product2, basket, false);

            Assert.False(basket.ApplicableDiscounts.FirstOrDefault(a => a.Id == 1).IsUsed);
        }

        [Fact]
        public void BasketService_AddProductToBasket_GiftShouldntChangeTotalValue()
        {
            var basketService = new BasketService(new DiscountService());

            Product product = new Product
            {
                Id = 1,
                Name = "Milk",
                Price = 1,
                ApplicableDiscounts = new List<Discount>(),
                BasketProducts = new List<BasketProduct>(),
                DiscountsForTriggering = new List<Discount>()
            };

            Basket basket = new Basket
            {
                Id = 1,
                TotalValue = 0,
                IsClosed = false,
                UserId = 1,
                ApplicableDiscounts = new List<ApplicableDiscount>(),
                BasketProducts = new List<BasketProduct>()
            };

            basket = basketService.AddProductToBasket(product, basket, true);

            Assert.Equal(0, basket.TotalValue);
        }

        [Fact]
        public void BasketService_RemoveProductFromBasket_GiftShouldntChangeTotalValue()
        {
            var basketService = new BasketService(new DiscountService());

            Product product = new Product
            {
                Id = 1,
                Name = "Milk",
                Price = 1,
                ApplicableDiscounts = new List<Discount>(),
                BasketProducts = new List<BasketProduct>(),
                DiscountsForTriggering = new List<Discount>()
            };

            Product product2 = new Product
            {
                Id = 2,
                Name = "Milk",
                Price = 1,
                ApplicableDiscounts = new List<Discount>(),
                BasketProducts = new List<BasketProduct>(),
                DiscountsForTriggering = new List<Discount>()
            };

            Discount discount = new Discount
            {
                Id = 1,
                Rate = 1,
                DiscountProductId = 2,
                DiscountProduct = product2,
                IsActive = true,
                TriggerProductId = 1,
                TriggerProduct = product,
                TriggerProductCount = 1,
                Type = DiscountTypeEnum.Gift
            };

            product.DiscountsForTriggering.Add(discount);
            product2.ApplicableDiscounts.Add(discount);

            Basket basket = new Basket
            {
                Id = 1,
                TotalValue = 1,
                IsClosed = false,
                UserId = 1,
                ApplicableDiscounts = new List<ApplicableDiscount>(),
                BasketProducts = new List<BasketProduct>()
            };

            var basketProduct = new BasketProduct
            {
                Id = 1,
                IsGift = false,
                BasketId = 1,
                ProductId = 1,
                Product = product,
                ProductCount = 1
            };

            product.BasketProducts.Add(basketProduct);
            basket.BasketProducts.Add(basketProduct);

            var basketProduct2 = new BasketProduct
            {
                Id = 2,
                IsGift = true,
                BasketId = 1,
                ProductId = 2,
                Product = product2,
                ProductCount = 1
            };

            product2.BasketProducts.Add(basketProduct2);
            basket.BasketProducts.Add(basketProduct2);

            basket.ApplicableDiscounts.Add(new ApplicableDiscount
            {
                Id = 1,
                Basket = basket,
                BasketId = 1,
                Discount = discount,
                DiscountId = 1,
                IsUsed = true
            });

            basket = basketService.RemoveProductFromBasket(product, basket, false);

            Assert.Equal(0, basket.TotalValue);
        }
    }
}
