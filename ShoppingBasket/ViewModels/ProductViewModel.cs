﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.ShoppingBasket.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class ProductViewModel
    {
        /// <summary>
        /// Product id.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Product name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Product price.
        /// </summary>
        public double Price { get; set; }

        /// <summary>
        /// Product count in basket.
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Is product added to basket as gift.
        /// </summary>
        public bool IsGift { get; set; }
    }
}
