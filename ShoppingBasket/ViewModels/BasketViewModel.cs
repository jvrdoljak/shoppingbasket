﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.ShoppingBasket.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class BasketViewModel
    {
        /// <summary>
        /// Basket id.
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// Basket total value.
        /// </summary>
        public double TotalValue { get; set; }
        
        /// <summary>
        /// Basket created on.
        /// </summary>
        public DateTime CreatedOn { get; set; }
        
        /// <summary>
        /// Basket modified on.
        /// </summary>
        public DateTime ModifiedOn { get; set; }
        
        /// <summary>
        /// Basket is closed.
        /// </summary>
        public bool IsClosed { get; set; }
        
        /// <summary>
        /// Collection of products in basket.
        /// </summary>
        public ICollection<ProductViewModel> Products { get; set; }
        
        /// <summary>
        /// Collection of discounts which related to basket.
        /// </summary>
        public ICollection<DiscountsViewModel> Discounts { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public BasketViewModel()
        {
            Products = new List<ProductViewModel>();
            Discounts = new List<DiscountsViewModel>();
        }
    }
}
