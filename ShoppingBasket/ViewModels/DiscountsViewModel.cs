﻿using App.ShoppingBasket.Enum;
using App.ShoppingBasket.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.ShoppingBasket.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class DiscountsViewModel
    {
        /// <summary>
        /// Discount id.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Product id on which discount is applied.
        /// </summary>
        public long ProductId { get; set; }

        /// <summary>
        /// Discount rate. If 1, it is gift.
        /// </summary>
        public double Rate { get; set; }

        /// <summary>
        /// Whether the discount has been used.
        /// </summary>
        public bool IsUsed { get; set; }
        
        /// <summary>
        /// Type of discount.
        /// </summary>
        public DiscountTypeEnum Type { get; set; }
    }
}
