﻿using Microsoft.EntityFrameworkCore;
using App.ShoppingBasket.Models;

public class AppDbContext : DbContext
{

    public AppDbContext()
    {

    }

    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Discount>()
            .HasOne(a => a.TriggerProduct)
            .WithMany(b => b.DiscountsForTriggering)
            .HasForeignKey(c => c.TriggerProductId);

        modelBuilder.Entity<Discount>()
            .HasOne(a => a.DiscountProduct)
            .WithMany(b => b.ApplicableDiscounts)
            .HasForeignKey(c => c.DiscountProductId);

        modelBuilder.Entity<BasketProduct>()
            .HasOne(a => a.Basket)
            .WithMany(b => b.BasketProducts)
            .HasForeignKey(c => c.BasketId);

        modelBuilder.Entity<BasketProduct>()
            .HasOne(a => a.Product)
            .WithMany(b => b.BasketProducts)
            .HasForeignKey(c => c.ProductId);

        modelBuilder.Entity<ApplicableDiscount>()
            .HasOne(a => a.Basket)
            .WithMany(b => b.ApplicableDiscounts)
            .HasForeignKey(c => c.BasketId);

        modelBuilder.Entity<ApplicableDiscount>()
            .HasOne(a => a.Discount)
            .WithMany(b => b.ApplicableDiscounts)
            .HasForeignKey(c => c.DiscountId);

        modelBuilder.Entity<User>()
            .HasIndex(a => a.Username)
            .IsUnique();
    }

    public DbSet<ApplicableDiscount> ApplicableDiscounts { get; set; }
    public DbSet<Basket> Baskets { get; set; }
    public DbSet<BasketProduct> BasketProducts { get; set; }
    public DbSet<Discount> Discounts { get; set; }
    public DbSet<Product> Product { get; set; }
    public DbSet<User> Users { get; set; }
}