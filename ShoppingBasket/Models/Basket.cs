﻿using System;
using System.Collections.Generic;

namespace App.ShoppingBasket.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class Basket
    {
        /// <summary>
        /// 
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public double TotalValue { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public ICollection<BasketProduct> BasketProducts { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public ICollection<ApplicableDiscount> ApplicableDiscounts { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public DateTime CreatedOn { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public DateTime ModifiedOn { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public long UserId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public User User { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public bool IsClosed { get; set; }

        public Basket()
        {
            BasketProducts = new List<BasketProduct>();
            ApplicableDiscounts = new List<ApplicableDiscount>();
        }
    }
}
