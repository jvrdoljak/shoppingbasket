﻿using System;

namespace App.ShoppingBasket.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class BasketProduct
    {
        /// <summary>
        /// 
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public long ProductId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public Product Product { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public long BasketId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public Basket Basket { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public int ProductCount { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public bool IsGift { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public DateTime CreatedOn { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public DateTime ModifiedOn { get; set; }
    }
}
