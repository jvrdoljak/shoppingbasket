﻿using App.ShoppingBasket.Enum;
using System;
using System.Collections.Generic;

namespace App.ShoppingBasket.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class Discount
    {
        /// <summary>
        /// 
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Id of product which is required to be in basket.
        /// </summary>
        public long TriggerProductId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Product TriggerProduct { get; set; }

        /// <summary>
        /// Number of trigger products to make the discount applicable.
        /// </summary>
        public int TriggerProductCount { get; set; }

        /// <summary>
        /// Id of product on which discount will be applied.
        /// </summary>
        public long DiscountProductId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Product DiscountProduct { get; set; }

        /// <summary>
        /// Discount rate. 1 is 100%, that is default value for gifts.
        /// </summary>
        public double Rate { get; set; }

        /// <summary>
        /// Is discount active.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Type of discount.
        /// </summary>
        public DiscountTypeEnum Type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ICollection<ApplicableDiscount> ApplicableDiscounts { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime ModifiedOn { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public Discount()
        {
            ApplicableDiscounts = new List<ApplicableDiscount>();
        }
    }
}
