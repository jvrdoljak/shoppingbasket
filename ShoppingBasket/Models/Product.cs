﻿using System;
using System.Collections.Generic;

namespace App.ShoppingBasket.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class Product
    {
        /// <summary>
        /// 
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public double Price { get; set; }

        /// <summary>
        /// List of discounts which could be applied.
        /// </summary>
        public ICollection<Discount> ApplicableDiscounts { get; set; }

        /// <summary>
        /// List of discounts which could be triggered.
        /// </summary>
        public ICollection<Discount> DiscountsForTriggering { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ICollection<BasketProduct> BasketProducts { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime ModifiedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Product()
        {
            ApplicableDiscounts = new List<Discount>();
            DiscountsForTriggering = new List<Discount>();
            BasketProducts = new List<BasketProduct>();
        }
    }
}
