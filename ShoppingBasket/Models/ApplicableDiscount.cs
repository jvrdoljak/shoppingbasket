﻿using System;

namespace App.ShoppingBasket.Models
{
    /// <summary>
    /// Discounts which could be applied on products in exaxt basket.
    /// </summary>
    public class ApplicableDiscount
    {
        /// <summary>
        /// 
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Discount id.
        /// </summary>
        public long DiscountId { get; set; }
 
        /// <summary>
        /// 
        /// </summary>
        public Discount Discount { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public long BasketId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public Basket Basket { get; set; }
        
        /// <summary>
        /// Is discount used in bakset.
        /// </summary>
        public bool IsUsed { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public DateTime CreatedOn { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public DateTime ModifiedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ApplicableDiscount()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="discount">Discount object</param>
        /// <param name="basket">Basket object</param>
        /// <param name="isUsed">true if discount is applied</param>
        public ApplicableDiscount(
            Discount discount,
            Basket basket,
            bool isUsed = false)
        {
            Discount = discount;
            Basket = basket;
            IsUsed = isUsed;
        }
    }
}
