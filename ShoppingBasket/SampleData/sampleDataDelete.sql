﻿DELETE FROM [dbo].[Product] WHERE [Name] = 'Butter' AND [Price] = 0.8; 
DELETE FROM [dbo].[Product] WHERE [Name] = 'Milk' AND [Price] = 1.15;
DELETE FROM [dbo].[Product] WHERE [Name] = 'Bread' AND [Price] = 1.00;

DELETE FROM [dbo].[Users] WHERE [Username] = 'sampleuser';