﻿using App.ShoppingBasket.Enum;
using App.ShoppingBasket.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.ShoppingBasket.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class DiscountService
    {
        /// <summary>
        /// 
        /// </summary>
        public DiscountService() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="discounts"></param>
        /// <param name="triggerProductCount"></param>
        /// <returns></returns>
        public List<Discount> GetDiscountsForTriggering(List<Discount> discounts, int triggerProductCount)
        {
            return discounts
                .Where(a => a.IsActive && (triggerProductCount % a.TriggerProductCount == 0))
                .ToList();
        }
    }
}
