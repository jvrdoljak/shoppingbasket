﻿using App.ShoppingBasket.Enum;
using App.ShoppingBasket.Models;
using App.ShoppingBasket.ViewModels;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.ShoppingBasket.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class BasketService
    {

        /// <summary>
        /// 
        /// </summary>
        private DiscountService _discountService { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="discountService"></param>
        public BasketService(DiscountService discountService)
        {
            _discountService = discountService;
        }

        /// <summary>
        /// Creates empty basket
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Basket Create(long userId)
        {
            var basket = new Basket();
            basket.TotalValue = 0;
            basket.UserId = userId;
            basket.IsClosed = false;
            basket.CreatedOn = DateTime.Now;
            basket.ModifiedOn = DateTime.Now;

            return basket;
        }

        /// <summary>
        /// Function for adding product in basket.
        /// </summary>
        /// <param name="product"></param>
        /// <param name="basket"></param>
        /// <param name="isGift"></param>
        /// <returns></returns>
        public Basket AddProductToBasket(Product product, Basket basket, bool isGift = false)
        {            
            var basketProduct = new BasketProduct();

            if (IsProductInBasket(basket.BasketProducts.ToList(), product.Id) && !isGift)
            {
                basketProduct = basket.BasketProducts.FirstOrDefault(a => a.ProductId == product.Id);

                basketProduct.ProductCount += 1;
            } else
            {
                basketProduct = CreateBasketProduct(basket.Id, product.Id, isGift);
                basket.BasketProducts.Add(basketProduct);
            }

            var discountsForTriggering = _discountService
                .GetDiscountsForTriggering(product.DiscountsForTriggering.ToList(), basketProduct.ProductCount);

            if(discountsForTriggering.Any())
            {
                foreach(var discount in discountsForTriggering)
                {
                    if (discount.Type == DiscountTypeEnum.Discount)
                    {
                        if(IsProductInBasket(basket.BasketProducts.ToList(), discount.DiscountProductId, isGift:false)) {
                            var applicableDiscount1 = basket.ApplicableDiscounts.Where(a => a.DiscountId == discount.Id).ToList();

                            if(applicableDiscount1 != null)
                            {
                                // je li broj iskoristenih popusta na tom proizvodu manji od broja tih proizvoda u kosarici
                                // ako je onda ga iskoristi automatski i spremi da je iskoristen
                                // ako nije onda ga spremi da nije iskoristen
                                if(applicableDiscount1.Count < basket.BasketProducts.FirstOrDefault(a => a.ProductId == discount.DiscountProductId && !a.IsGift).ProductCount)
                                {
                                    basket.TotalValue -= discount.Rate * discount.DiscountProduct.Price;
                                    basket.ApplicableDiscounts
                                        .Add(new ApplicableDiscount(discount, basket, true));
                                } else
                                {
                                    basket.ApplicableDiscounts
                                        .Add(new ApplicableDiscount(discount, basket, false));
                                }
                            }                                
                        } else
                        {
                            basket.ApplicableDiscounts.Add(new ApplicableDiscount(discount, basket));
                        }
                    } else if (discount.Type == DiscountTypeEnum.Gift)
                    {
                        basket = AddProductToBasket(discount.DiscountProduct, basket, true);
                        basket.ApplicableDiscounts.Add(new ApplicableDiscount(discount, basket, true));
                    }
                }
            }

            if (!isGift)
            {
                basket.TotalValue += product.Price;

                // ako postoji popust koji ranije nije iskoristen iskoristi ga 
                var applicableDiscounts = basket.ApplicableDiscounts
                    .Where(a => !a.IsUsed && a.Discount.DiscountProductId == product.Id)
                    .FirstOrDefault();
                if (applicableDiscounts != null)
                {
                    basket.TotalValue -= applicableDiscounts.Discount.Rate * product.Price;
                    applicableDiscounts.IsUsed = true;
                }
            }

            return basket;
        }

        /// <summary>
        /// Remove product and all his discounts from basket.
        /// </summary>
        /// <param name="product"></param>
        /// <param name="basket"></param>
        /// <param name="isGift"></param>
        /// <returns></returns>
        public Basket RemoveProductFromBasket(Product product, Basket basket, bool isGift = false)
        {
            var basketProduct = basket.BasketProducts.Where(a => a.ProductId == product.Id && a.IsGift == isGift).FirstOrDefault();

            if (!isGift)
            {
                basket.TotalValue -= product.Price;

                var applicableDiscounts = basket.ApplicableDiscounts
                    .Where(a => a.IsUsed && a.Discount.DiscountProductId == product.Id && a.Discount.Type != DiscountTypeEnum.Gift)
                    .ToList();
                if (applicableDiscounts.Any())
                {
                    if(applicableDiscounts.Count >= basketProduct.ProductCount)
                    {
                        var applicableDiscount = applicableDiscounts.FirstOrDefault();
                        basket.TotalValue += applicableDiscount.Discount.Rate * product.Price;
                        applicableDiscount.IsUsed = false;
                    }   
                }
            }

            // pokupi sve popuste na koje je utjecao ovaj proizvod
            // group by discount, because of one one new product can trigger one discount once
            var appliedDdiscountsForRemove = basket.ApplicableDiscounts
                .Where(a => !basketProduct.IsGift && (a.Discount.TriggerProductId == product.Id) && (basketProduct.ProductCount % a.Discount.TriggerProductCount == 0))
                .OrderBy(a => a.IsUsed)
                .GroupBy(a => a.DiscountId)
                .ToList();

            if(appliedDdiscountsForRemove.Any())
            {
                //foreach group of identical discounts
                foreach(var a in appliedDdiscountsForRemove)
                {
                    // it alway will be unused discount if exist
                    var appliedDiscount = a.FirstOrDefault();

                    if (appliedDiscount.Discount.Type == DiscountTypeEnum.Discount)
                    {
                        if (IsProductInBasket(basket.BasketProducts.ToList(), appliedDiscount.Discount.DiscountProductId, isGift: false))
                        {
                            basket.TotalValue += appliedDiscount.Discount.DiscountProduct.Price * appliedDiscount.Discount.Rate;
                            basket.ApplicableDiscounts.Remove(appliedDiscount);
                        }
                        else
                        {
                            basket.ApplicableDiscounts.Remove(appliedDiscount);
                        }
                    }
                    else if (appliedDiscount.Discount.Type == DiscountTypeEnum.Gift)
                    {
                        basket = RemoveProductFromBasket(appliedDiscount.Discount.DiscountProduct, basket, true);
                        basket.ApplicableDiscounts.Remove(appliedDiscount);
                    }
                }
            }

            if(basketProduct.ProductCount > 1)
            {
                basketProduct.ProductCount -= 1;
                //basket.BasketProducts.FirstOrDefault(a => a.ProductId == product.Id && a.IsGift == isGift).ProductCount -= 1;
            }
            else
            {
                basket.BasketProducts.Remove(basketProduct);
            }

            return basket;
        }

        /// <summary>
        /// Map basket data to BasketViewModel.
        /// </summary>
        /// <param name="basket"></param>
        /// <returns></returns>
        public BasketViewModel MapBasketViewModel(Basket basket)
        {
            return new BasketViewModel()
            {
                Id = basket.Id,
                IsClosed = basket.IsClosed,
                TotalValue = basket.TotalValue,
                CreatedOn = basket.CreatedOn,
                ModifiedOn = basket.ModifiedOn,
                Products = basket.BasketProducts.Select(a => new ProductViewModel
                {
                    Id = a.Product.Id,
                    Count = a.ProductCount,
                    Name = a.Product.Name,
                    Price = a.Product.Price,
                    IsGift = a.IsGift
                }).ToList(),
                Discounts = basket.ApplicableDiscounts.Select(a => new DiscountsViewModel
                {
                    Id = a.Discount.Id,
                    Rate = a.Discount.Rate,
                    IsUsed = a.IsUsed,
                    Type = a.Discount.Type,
                    ProductId = a.Discount.DiscountProductId
                }).ToList()
            };
        }

        /// <summary>
        /// Is product already exists in Basket.
        /// </summary>
        /// <param name="basketProducts"></param>
        /// <param name="productId"></param>
        /// <param name="isGift"></param>
        /// <returns></returns>
        public bool IsProductInBasket(List<BasketProduct> basketProducts, long productId, bool isGift = false)
        {
            return basketProducts.Any(a => a.ProductId == productId && !isGift);
        }

        /// <summary>
        /// Calculate number of products in basket.
        /// </summary>
        /// <param name="basketProducts"></param>
        /// <returns></returns>
        public int GetProductsCount(List<BasketProduct> basketProducts)
        {
            int count = 0;
            foreach (var basketProduct in basketProducts)
            {
                count += basketProduct.ProductCount;
            }
            return count;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="basket"></param>
        /// <param name="product"></param>
        /// <param name="isGift"></param>
        /// <returns></returns>
        public BasketProduct CreateBasketProduct(long basketId, long productId, bool isGift)
        {
            BasketProduct basketProduct = new BasketProduct();

            basketProduct.BasketId = basketId;
            basketProduct.ProductId = productId;
            basketProduct.CreatedOn = DateTime.Now;
            basketProduct.ModifiedOn = DateTime.Now;
            basketProduct.ProductCount = 1;
            basketProduct.IsGift = isGift;

            return basketProduct;
        }
    }
}
