﻿using System;
using System.Linq;
using System.Threading.Tasks;
using App.ShoppingBasket.Models;
using App.ShoppingBasket.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Web.ShoppingBasket.Controllers
{
    [ApiController]
    [Route("api/product")]
    public class ProductController : ControllerBase
    {
        public AppDbContext _appDbContext { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="basketService"></param>
        /// <param name="productService"></param>
        public ProductController(
            AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        /// <summary>
        /// List of all products.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Index()
        {
            return Ok(_appDbContext.Product.ToList());
        }

        /// <summary>
        /// Read product data.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{productId}")]
        public IActionResult Read(long productId)
        {
            Product product = _appDbContext.Product.FirstOrDefault(a => a.Id == productId);

            if(product == null)
            {
                return NotFound($"Product with id: {productId} is not found.");
            }
            
            return Ok(product);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> Create([FromBody] Product product)
        {
            if (product == null)
            {
                return BadRequest();
            }

            _appDbContext.Product.Add(new Product
            {
                Name = product.Name,
                Price = product.Price,
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now
            });
            await _appDbContext.SaveChangesAsync();
            
            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{productId}")]
        public async Task<IActionResult> Update(long productId, [FromBody] Product product)
        {
            Product p = await _appDbContext.Product.FirstOrDefaultAsync(a => a.Id == productId);

            if(p == null)
            {
                return NotFound($"Product with id {productId} is not found.");
            }

            if(product == null)
            {
                return BadRequest();
            }

            p.Name = product.Name;
            p.Price = product.Price;
            p.ModifiedOn = DateTime.Now;

            _appDbContext.Update(p);
            await _appDbContext.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// Deletes product.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{productId}")]
        public async Task<IActionResult> Delete(long productId)
        {
            Product product = await _appDbContext.Product.FirstOrDefaultAsync(a => a.Id == productId);

            if(product == null)
            {
                return NotFound($"Product with id {productId} is not found.");
            }

            var discounts = await _appDbContext.Discounts.Where(a => (a.TriggerProductId == productId) || (a.DiscountProductId == productId)).ToListAsync();

            _appDbContext.RemoveRange(discounts);
            _appDbContext.Remove(product);
            await _appDbContext.SaveChangesAsync();

            return Ok();
        }
    }
}
