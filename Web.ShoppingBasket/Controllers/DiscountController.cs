﻿using System;
using System.Linq;
using System.Threading.Tasks;
using App.ShoppingBasket.Enum;
using App.ShoppingBasket.Models;
using App.ShoppingBasket.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core.Features;
using Microsoft.EntityFrameworkCore;

namespace Web.ShoppingBasket.Controllers
{
    [ApiController]
    [Route("api/discount")]
    public class DiscountController : ControllerBase
    {
        private AppDbContext _appDbContext { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="basketService"></param>
        /// <param name="productService"></param>
        public DiscountController(
            AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        /// <summary>
        /// List of all discounts.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Index()
        {
            return Ok(_appDbContext.Discounts.ToList());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="discountId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{discountId}")]
        public async Task<IActionResult> Read(long discountId)
        {
            Discount discount = await _appDbContext.Discounts.FirstOrDefaultAsync(a => a.Id == discountId);

            if(discount == null)
            {
                return NotFound($"Discount with id {discountId} is not found");
            }

            return Ok(discount);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="discount"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Discount discount)
        {
            if(discount == null || !_appDbContext.Product.Any(a => a.Id == discount.DiscountProductId) || !_appDbContext.Product.Any(a => a.Id == discount.TriggerProductId))
            {
                return BadRequest();
            }

            if(discount.Type == DiscountTypeEnum.Gift)
            {
                discount.Rate = 1;
            }

            discount.IsActive = true;
            discount.CreatedOn = DateTime.Now;
            discount.ModifiedOn = DateTime.Now;

            _appDbContext.Add(discount);
            await _appDbContext.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="discountId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{discountId}")]
        public async Task<IActionResult> Delete(long discountId)
        {
            Discount discount = await _appDbContext.Discounts.FirstOrDefaultAsync(a => a.Id == discountId);

            if(discount == null)
            {
                return NotFound($"Discount with id {discountId} is not found.");
            }

            _appDbContext.Remove(discount);

            await _appDbContext.SaveChangesAsync();

            return Ok();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="discountId"></param>
        /// <param name="discount"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{discountId}")]
        public async Task<IActionResult> Update(long discountId, [FromBody] Discount discount)
        {
            Discount d = await _appDbContext.Discounts.FirstOrDefaultAsync(a => a.Id == discountId);
            
            if(d == null)
            {
                NotFound($"Discount with id: {discountId} is not found.");
            }

            if(discount == null)
            {
                return BadRequest();
            }

            if(discount.Type != DiscountTypeEnum.Gift && ((discount.Rate <= 0) || (discount.Rate >= 1)))
            {
                return BadRequest($"Discount rate: {discount.Rate}, should be between 0 and 1.");
            }

            d.DiscountProductId = discount.DiscountProductId;
            d.IsActive = discount.IsActive;
            d.TriggerProductId = discount.TriggerProductId;
            d.TriggerProductCount = discount.TriggerProductCount;
            d.Type = discount.Type;
            d.Rate =  discount.Type == DiscountTypeEnum.Gift ? 1 : discount.Rate;
            d.ModifiedOn = DateTime.Now;

            _appDbContext.Update(d);
            await _appDbContext.SaveChangesAsync();

            return Ok();
        }
    }
}
