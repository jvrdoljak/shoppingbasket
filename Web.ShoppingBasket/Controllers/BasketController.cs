﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.ShoppingBasket.Models;
using App.ShoppingBasket.Services;
using App.ShoppingBasket.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Web.ShoppingBasket.Controllers
{
    [ApiController]
    [Route("api/basket")]
    public class BasketController : ControllerBase
    {
        private AppDbContext _appDbContext { get; set; }
        private BasketService _basketService { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="basketService"></param>
        public BasketController(
            AppDbContext appDbContext,
            BasketService basketService)
        {
            _appDbContext = appDbContext;
            _basketService = basketService;
        }

        /// <summary>
        /// List of all baskets.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Index()
        {
            List<Basket> baskets = new List<Basket>();
            List <BasketViewModel> basketViewModels = new List<BasketViewModel>();

            baskets = _appDbContext.Baskets
                .Include(b => b.ApplicableDiscounts)
                    .ThenInclude(f => f.Discount)
                .Include(c => c.BasketProducts)
                    .ThenInclude(d => d.Product)
                .ToList();

            if(baskets.Any())
            {
                foreach(var basket in baskets)
                {
                    basketViewModels.Add(_basketService.MapBasketViewModel(basket));
                }
            }

            return Ok(basketViewModels);
        }

        /// <summary>
        /// Read basket data, with all products and discounts.
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{username}")]
        public async Task<IActionResult> Read(string username)
        {
            Basket basket = null;

            User user = await _appDbContext.Users
                .FirstOrDefaultAsync(a => a.Username == username);

            if (user == null)
            {
                return NotFound($"User with username {username} is not found");
            }

            basket = await _appDbContext.Baskets
                .Include(b => b.ApplicableDiscounts)
                    .ThenInclude(f => f.Discount)
                .Include(c => c.BasketProducts)
                    .ThenInclude(d => d.Product)
                .Where(a => a.UserId == user.Id && !a.IsClosed)
                .FirstOrDefaultAsync();

            if(basket == null) 
            {
                return NotFound($"User with username {username} does not have opened basket.");
            }
            
            return Ok(_basketService.MapBasketViewModel(basket));
        }

        /// <summary>
        /// Creates basket.
        /// </summary>
        /// <param name="username">User who owns basket.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("create/{username}")]
        public async Task<IActionResult> Create(string username)
        {
            Basket basket = null;

            User user = await _appDbContext.Users
                .FirstOrDefaultAsync(a => a.Username == username);

            if (user == null)
            {
                return NotFound($"User with username {username} is not found");
            }

            if (_appDbContext.Baskets.Any(a => a.UserId == user.Id && !a.IsClosed))
            {
                return BadRequest($"User with username {username} already have opened basket.");
            }
            else
            {
                basket = _basketService.Create(user.Id);

                _appDbContext.Add(basket);
                _appDbContext.SaveChanges();
            }

            return Ok();
        }

        /// <summary>
        /// Deletes basket.
        /// </summary>
        /// <param name="username">User who owns basket.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delete/{username}")]
        public async Task<IActionResult> Delete(string username)
        {
            Basket basket = null;

            User user = await _appDbContext.Users
                .FirstOrDefaultAsync(a => a.Username == username);

            if (user == null)
            {
                return NotFound($"User with username {username} is not found");
            }

            basket = await _appDbContext.Baskets.FirstOrDefaultAsync(a => a.UserId == user.Id);

            if (basket != null)
            {
                _appDbContext.Baskets.Remove(basket);
                await _appDbContext.SaveChangesAsync();
            }
            else
            {
                return Ok("Basket doesn't exist");
            }

            return Ok();
        }

        /// <summary>
        /// Add product to basket.
        /// </summary>
        /// <param name="username">User who owns basket.</param>
        /// <param name="productId">Id of product which should be added in basket.</param>
        /// <returns></returns>
        [HttpPut]
        [Route("addproduct/{username}/{productId}")]
        public async Task<IActionResult> AddProductAsync(string username, long productId)
        {
            Basket basket = null;

            User user = await _appDbContext.Users
                .FirstOrDefaultAsync(a => a.Username == username); 
            
            if(user == null)
            {
                return NotFound($"User with username {username} is not found");
            }

            Product product = _appDbContext.Product
                .Include(a => a.DiscountsForTriggering)
                    .ThenInclude(b => b.DiscountProduct)
                .FirstOrDefault(a => a.Id == productId);

            if (product == null)
            {
                return NotFound("Product is not found");
            }

            // if opened basket exists, then get it from database
            // if don't exist, then create new basket
            if(_appDbContext.Baskets.Any(a => a.UserId == user.Id && !a.IsClosed))
            {
                basket = await _appDbContext.Baskets
                .Include(a => a.BasketProducts)
                .Include(d => d.ApplicableDiscounts)
                    .ThenInclude(f => f.Discount)
                .Include(f => f.User)
                .Where(a => a.UserId == user.Id && !a.IsClosed)
                .FirstOrDefaultAsync();
            } else
            {
                basket = _basketService.Create(user.Id);

                _appDbContext.Add(basket);
                _appDbContext.SaveChanges();

                basket = await _appDbContext.Baskets
                .Include(a => a.BasketProducts)
                .Include(d => d.ApplicableDiscounts)
                    .ThenInclude(f => f.Discount)
                .Include(f => f.User)
                .Where(a => a.UserId == user.Id && !a.IsClosed)
                .FirstOrDefaultAsync();
            }
            
            basket = _basketService.AddProductToBasket(product, basket);

            _appDbContext.Update(basket);
            _appDbContext.SaveChanges();

            return Ok("Product is successfully added"); 
        }

        /// <summary>
        /// Remove product from basket.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("removeproduct/{username}/{productId}")]
        public async Task<IActionResult> RemoveProductAsync(string username, long productId)
        {
            Basket basket = null;

            User user = await _appDbContext.Users
                .FirstOrDefaultAsync(a => a.Username == username);

            if (user == null)
            {
                return NotFound($"User with username {username} is not found");
            }

            Product product = _appDbContext.Product.FirstOrDefault(a => a.Id == productId);

            if (product == null)
            {
                return NotFound("Product is not found");
            }

            if (_appDbContext.Baskets.Any(a => a.UserId == user.Id && !a.IsClosed))
            {
                basket = await _appDbContext.Baskets
                .Include(a => a.BasketProducts)
                .Include(d => d.ApplicableDiscounts)
                    .ThenInclude(f => f.Discount)
                        .ThenInclude(f => f.DiscountProduct)
                .Include(f => f.User)
                .Where(a => a.UserId == user.Id && !a.IsClosed)
                .FirstOrDefaultAsync();
            }
            else
            {
                return NotFound($"User with username {username} doesn't have opened basket.");
            }

            if (basket.BasketProducts.Any(a => a.ProductId == product.Id && !a.IsGift))
            {
                basket = _basketService.RemoveProductFromBasket(product, basket);
            } else
            {
                return BadRequest($"Product (Id: {product.Id}, Name: {product.Name}, Price: {product.Price.ToString()}) isn't in basket.");
            }

            _appDbContext.Update(basket);
            _appDbContext.SaveChanges();

            return Ok($"Product (Name: {product.Name}, Price: {product.Price.ToString()}) is successfully removed");
        }

        /// <summary>
        /// Closes basket.
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("close/{username}")]
        public async Task<IActionResult> CloseBasket(string username)
        {
            Basket basket = null;

            User user = await _appDbContext.Users
                .FirstOrDefaultAsync(a => a.Username == username);

            if (user == null)
            {
                return NotFound($"User with username {username} is not found");
            }

            if (_appDbContext.Baskets.Any(a => a.UserId == user.Id))
            {
                basket = await _appDbContext.Baskets
                .Include(a => a.BasketProducts)
                .Include(d => d.ApplicableDiscounts)
                    .ThenInclude(f => f.Discount)
                        .ThenInclude(f => f.DiscountProduct)
                .Include(f => f.User)
                .Where(a => a.UserId == user.Id && !a.IsClosed)
                .FirstOrDefaultAsync();
            }
            else
            {
                return BadRequest($"User with username {username} doesn't have opened basket.");
            }

            basket.IsClosed = true;

            _appDbContext.Update(basket);
            _appDbContext.SaveChanges();

            return Ok();
        }
    }
}
